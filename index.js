// imports
const { RTMClient } = require('@slack/rtm-api');
const { WebClient } = require('@slack/web-api');
const { config } = require('./configs/config');
const path = require('path');
const low = require('lowdb');
const moment = require('moment-timezone');
const FileSync = require('lowdb/adapters/FileSync');
const functions = require('./functions');
const rtm = new RTMClient(config.botToken);
const web = new WebClient(config.botToken);
const dirname = path.dirname(require.main.filename);
const adapter = new FileSync(dirname+'/db.json');
const db = low(adapter);
let { botID, botName, teamDomain } = '';

db.defaults({
  imList: [],
  imListUpdatedAt: moment(),
  restaurants: [],
}).write();

(async () => {
  // Connect to Slack
  const { self, team } = await rtm.start();
  // update imList
  const startTime = moment(db.get('imListUpdatedAt').value());
  const endTime = moment();
  const duration = endTime.diff(startTime);
  const FIVE_MIN = (5 * 60 * 1000);
  if (duration > FIVE_MIN) {
    const imList = await web.im.list({
      limit: 999,
    });
    if (imList.ims && imList.ok) {
      console.log('imList Updated.');
      db.set('imList', imList.ims).write();
      db.set('imListUpdatedAt', moment()).write();
    }
  }
  // log bot identity
  botID = self.id;
  botName = self.name;
  teamDomain = team.domain+'.slack.com';
  console.log(`Successfully logged in as ${botName}(${botID}) on ${teamDomain}`);
})();

//  Debugging
// rtm.on('slack_event', (event) => {
//   console.log(event);
// });

// When new member joins the slackboard
rtm.on('team_join', async (event) => {
  console.log('imList Updated.');
  const imList = await web.im.list({
    limit: 999,
  });
  if (imList.ims && imList.ok) {
    console.log('imList Updated.');
    db.set('imList', imList.ims).write();
  }
});

// Channel Messages
rtm.on('message', (data) => {
  functions.runMain(rtm, web, data, db);
});
