const config = {
  // Authentication
  botToken: '',
  // Settings
  prefix: 'm ',
  addRestaurantTimeout: 30000,
};

module.exports = {
  config,
};
