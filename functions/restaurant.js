// const moment = require('moment-timezone');
// const request = require('request');
const { config } = require('../configs/config');
const u = require('./utils');
// add restaurant
let addRestaurantStep = [];
let addRestaurantisError = [];
let restaurantStepResetTimeout = [];
let restaurantData = [];
// update restaurant
let updateRestaurantStep = [];
let updateRestaurantisError = [];
let updateRestaurantStepResetTimeout = [];
let updateRestaurantData = [];
let updateRestaurantID = [];

// addRestaurant adds restaurant to the database;
exports.addRestaurant = (rtm, data, db) => {
  if (u.checkCommands(data, ['add food', 'add makan', 'add restaurant'], false)) {
    if (!u.isMessageDM(db, data)) return;
    if (updateRestaurantStep[data.user]) return;
    if (!addRestaurantStep[data.user]) addRestaurantStep[data.user] = 1;
    addRestaurantFunction(rtm, data, db, u.restaurantStruct());
  } else if (u.checkCommands(data, ['cancel', 'stop'], false)) {
    if (!u.isMessageDM(db, data)) return;
    if (addRestaurantStep[data.user]) {
      clearTimeout(restaurantStepResetTimeout[data.user]);
      delete addRestaurantStep[data.user];
      delete restaurantStepResetTimeout[data.user];
      delete restaurantData[data.user];
      rtm.sendMessage('Adding restaurant has been cancelled.', data.channel);
    }
  } else if (addRestaurantStep[data.user] > 1) {
    if (!u.isMessageDM(db, data)) return;
    addRestaurantFunction(rtm, data, db, restaurantData[data.user]);
  }
};
function addRestaurantFunction(rtm, data, db, restaurant) {
  addRestaurantTimeout(rtm, data);
  switch (addRestaurantStep[data.user]) {
  case 1:
    console.log(1);
    addRestaurantStep[data.user] = 2;
    rtm.sendMessage('What is the name of the restaurant?', data.channel);
    restaurantData[data.user] = restaurant;
    break;
  case 2:
    console.log(2);
    addRestaurantStep[data.user] = 3;
    rtm.sendMessage('Where is it located at?', data.channel);
    restaurantData[data.user] = restaurant;
    restaurantData[data.user].name = data.text;
    break;
  case 3:
    console.log(3);
    addRestaurantStep[data.user] = 4;
    rtm.sendMessage('Is the restaurant inside Oasis Square? [Yes/No]', data.channel);
    restaurantData[data.user] = restaurant;
    restaurantData[data.user].location = data.text;
    break;
  case 4:
    console.log(4);
    restaurantData[data.user] = restaurant;
    let locationType = '';
    if (data.text.toLowerCase().includes('yes') || data.text.toLowerCase() === 'y') {
      locationType = 'in';
    } else if (data.text.toLowerCase().includes('no') || data.text.toLowerCase() === 'n') {
      locationType = 'out';
    }
    if (locationType !== '') {
      restaurantData[data.user].locationType = locationType;
      addRestaurantStep[data.user] = 5;
      rtm.sendMessage('How difficult is it to find a parking spot? [Easy/Moderate/Hard]', data.channel);
      delete addRestaurantisError[data.user];
    } else {
      addRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Yes/No]', data.channel);
    }
    break;
  case 5:
    console.log(5);
    restaurantData[data.user] = restaurant;
    if (data.text.toLowerCase() === 'easy' || data.text.toLowerCase() === 'moderate' || data.text.toLowerCase() === 'hard') {
      restaurantData[data.user].parking = data.text;
      addRestaurantStep[data.user] = 6;
      rtm.sendMessage('Is this restaurant cheap? [Yes/No]', data.channel);
      delete addRestaurantisError[data.user];
    } else {
      addRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Easy/Moderate/Hard]', data.channel);
    }
    break;
  case 6:
    console.log(6);
    restaurantData[data.user] = restaurant;
    let cheap = '';
    if (data.text.toLowerCase().includes('yes') || data.text.toLowerCase() === 'y') {
      cheap = true;
    } else if (data.text.toLowerCase().includes('no') || data.text.toLowerCase() === 'n') {
      cheap = false;
    }
    if (cheap !== '') {
      restaurantData[data.user].cheap = cheap;
      addRestaurantStep[data.user] = 7;
      rtm.sendMessage('Is it a halal place? [Yes/No/Unsure]', data.channel);
      delete addRestaurantisError[data.user];
    } else {
      addRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Yes/No]', data.channel);
    }
    break;
  case 7:
    console.log(7);
    restaurantData[data.user] = restaurant;
    let halal = '';
    if (data.text.toLowerCase().includes('yes') || data.text.toLowerCase() === 'y') {
      halal = 'yes';
    } else if (data.text.toLowerCase().includes('no') || data.text.toLowerCase() === 'n') {
      halal = 'no';
    } else if (data.text.toLowerCase().includes('unsure') || data.text.toLowerCase() === 'unsure') {
      halal = 'unsure';
    }
    if (halal !== '') {
      restaurantData[data.user].halal = halal;
      addRestaurantStep[data.user] = 8;
      rtm.sendMessage('Does it have air conditioner? [Yes/No]', data.channel);
      delete addRestaurantisError[data.user];
    } else {
      addRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Yes/No/Unsure]', data.channel);
    }
    break;
  case 8:
    console.log(8);
    restaurantData[data.user] = restaurant;
    let aircond = '';
    if (data.text.toLowerCase().includes('yes') || data.text.toLowerCase() === 'y') {
      aircond = true;
    } else if (data.text.toLowerCase().includes('no') || data.text.toLowerCase() === 'n') {
      aircond = false;
    }
    if (aircond !== '') {
      restaurantData[data.user].aircond = aircond;
      rtm.sendMessage(`*${restaurantData[data.user].name}* successfully added.\n`, data.channel);
      rtm.sendMessage(u.formatRestaurantString(restaurantData[data.user]), data.channel);
      db.get('restaurants').push(restaurantData[data.user]).write();
      console.log(restaurantData[data.user]);
      clearTimeout(restaurantStepResetTimeout[data.user]);
      delete addRestaurantStep[data.user];
      delete restaurantData[data.user];
      delete addRestaurantisError[data.user];
      delete restaurantStepResetTimeout[data.user];
    } else {
      addRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Yes/No]', data.channel);
    }
    break;
  default:
    break;
  }
}
function addRestaurantTimeout(rtm, data) {
  clearTimeout(restaurantStepResetTimeout[data.user]);
  restaurantStepResetTimeout[data.user] = setTimeout(() => {
    delete addRestaurantStep[data.user];
    delete restaurantStepResetTimeout[data.user];
    delete restaurantData[data.user];
    rtm.sendMessage(`<@${data.user}>, No reponse received. Adding restaurant has timeout.`, data.channel);
  }, config.addRestaurantTimeout);
}

// updateRestaurant updates restaurant from the database;
exports.updateRestaurant = (rtm, data, db) => {
  const command = u.checkCommands(data, ['update food', 'update makan', 'update restaurant'], false);
  if (command.data) {
    if (!u.isMessageDM(db, data)) return;
    if (addRestaurantStep[data.user]) return;
    if (!updateRestaurantStep[data.user]) updateRestaurantStep[data.user] = 1;
    if (!db.get('restaurants').find({ id: command.data }).value()) {
      rtm.sendMessage(`Cannot find restaurant with the ID of *${command.data}*.`, data.channel);
    };
    updateRestaurantID[data.user] = command.data;
    updateRestaurantFunction(rtm, data, db, u.restaurantStruct({ id: command.data }));
  } else if (u.checkCommands(data, ['cancel', 'stop'], false)) {
    if (!u.isMessageDM(db, data)) return;
    if (updateRestaurantStep[data.user]) {
      clearTimeout(updateRestaurantStepResetTimeout[data.user]);
      delete updateRestaurantStep[data.user];
      delete updateRestaurantStepResetTimeout[data.user];
      delete updateRestaurantData[data.user];
      delete updateRestaurantID[data.user];
      rtm.sendMessage('Update restaurant has been cancelled.', data.channel);
    }
  } else if (updateRestaurantStep[data.user] > 1) {
    if (!u.isMessageDM(db, data)) return;
    updateRestaurantFunction(rtm, data, db, updateRestaurantData[data.user]);
  }
};
function updateRestaurantFunction(rtm, data, db, restaurant) {
  updateRestaurantTimeout(rtm, data);
  switch (updateRestaurantStep[data.user]) {
  case 1:
    console.log(1);
    updateRestaurantStep[data.user] = 2;
    rtm.sendMessage('What is the name of the restaurant?', data.channel);
    updateRestaurantData[data.user] = restaurant;
    break;
  case 2:
    console.log(2);
    updateRestaurantStep[data.user] = 3;
    rtm.sendMessage('Where is it located at?', data.channel);
    updateRestaurantData[data.user] = restaurant;
    updateRestaurantData[data.user].name = data.text;
    break;
  case 3:
    console.log(3);
    updateRestaurantStep[data.user] = 4;
    rtm.sendMessage('Is the restaurant inside Oasis Square? [Yes/No]', data.channel);
    updateRestaurantData[data.user] = restaurant;
    updateRestaurantData[data.user].location = data.text;
    break;
  case 4:
    console.log(4);
    updateRestaurantData[data.user] = restaurant;
    let locationType = '';
    if (data.text.toLowerCase().includes('yes') || data.text.toLowerCase() === 'y') {
      locationType = 'in';
    } else if (data.text.toLowerCase().includes('no') || data.text.toLowerCase() === 'n') {
      locationType = 'out';
    }
    if (locationType !== '') {
      updateRestaurantData[data.user].locationType = locationType;
      updateRestaurantStep[data.user] = 5;
      rtm.sendMessage('How difficult is it to find a parking spot? [Easy/Moderate/Hard]', data.channel);
      delete updateRestaurantisError[data.user];
    } else {
      updateRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Yes/No]', data.channel);
    }
    break;
  case 5:
    console.log(5);
    updateRestaurantData[data.user] = restaurant;
    if (data.text.toLowerCase() === 'easy' || data.text.toLowerCase() === 'moderate' || data.text.toLowerCase() === 'hard') {
      updateRestaurantData[data.user].parking = data.text;
      updateRestaurantStep[data.user] = 6;
      rtm.sendMessage('Is this restaurant cheap? [Yes/No]', data.channel);
      delete updateRestaurantisError[data.user];
    } else {
      updateRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Easy/Moderate/Hard]', data.channel);
    }
    break;
  case 6:
    console.log(6);
    updateRestaurantData[data.user] = restaurant;
    let cheap = '';
    if (data.text.toLowerCase().includes('yes') || data.text.toLowerCase() === 'y') {
      cheap = true;
    } else if (data.text.toLowerCase().includes('no') || data.text.toLowerCase() === 'n') {
      cheap = false;
    }
    if (cheap !== '') {
      updateRestaurantData[data.user].cheap = cheap;
      updateRestaurantStep[data.user] = 7;
      rtm.sendMessage('Is it a halal place? [Yes/No/Unsure]', data.channel);
      delete updateRestaurantisError[data.user];
    } else {
      updateRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Yes/No]', data.channel);
    }
    break;
  case 7:
    console.log(7);
    updateRestaurantData[data.user] = restaurant;
    let halal = '';
    if (data.text.toLowerCase().includes('yes') || data.text.toLowerCase() === 'y') {
      halal = 'yes';
    } else if (data.text.toLowerCase().includes('no') || data.text.toLowerCase() === 'n') {
      halal = 'no';
    } else if (data.text.toLowerCase().includes('unsure') || data.text.toLowerCase() === 'unsure') {
      halal = 'unsure';
    }
    if (halal !== '') {
      updateRestaurantData[data.user].halal = halal;
      updateRestaurantStep[data.user] = 8;
      rtm.sendMessage('Does it have air conditioner? [Yes/No]', data.channel);
      delete updateRestaurantisError[data.user];
    } else {
      updateRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Yes/No/Unsure]', data.channel);
    }
    break;
  case 8:
    console.log(8);
    updateRestaurantData[data.user] = restaurant;
    let aircond = '';
    if (data.text.toLowerCase().includes('yes') || data.text.toLowerCase() === 'y') {
      aircond = true;
    } else if (data.text.toLowerCase().includes('no') || data.text.toLowerCase() === 'n') {
      aircond = false;
    }
    if (aircond !== '') {
      updateRestaurantData[data.user].aircond = aircond;
      rtm.sendMessage(`*${updateRestaurantData[data.user].name}* successfully updated.\n`, data.channel);
      rtm.sendMessage(u.formatRestaurantString(updateRestaurantData[data.user]), data.channel);
      db.get('restaurants').find({ id: updateRestaurantID[data.user] }).assign(updateRestaurantData[data.user]).write();
      console.log(updateRestaurantData[data.user]);
      clearTimeout(updateRestaurantStepResetTimeout[data.user]);
      delete updateRestaurantStep[data.user];
      delete updateRestaurantData[data.user];
      delete updateRestaurantisError[data.user];
      delete updateRestaurantStepResetTimeout[data.user];
      delete updateRestaurantID[data.user];
    } else {
      updateRestaurantisError[data.user] = true;
      rtm.sendMessage('Invalid response. [Yes/No]', data.channel);
    }
    break;
  default:
    break;
  }
}
function updateRestaurantTimeout(rtm, data) {
  clearTimeout(updateRestaurantStepResetTimeout[data.user]);
  updateRestaurantStepResetTimeout[data.user] = setTimeout(() => {
    delete updateRestaurantStep[data.user];
    delete updateRestaurantStepResetTimeout[data.user];
    delete updateRestaurantData[data.user];
    rtm.sendMessage(`<@${data.user}>, No reponse received. Update restaurant has timeout.`, data.channel);
  }, config.updateRestaurantTimeout);
}

// deleteRestaurant deletes a restaurant from the database
exports.deleteRestaurant = (rtm, data, db) => {
  const command = u.checkCommands(data, ['delete food', 'delete makan', 'delete restaurant'], false);
  if (command) {
    if (!u.isMessageDM(db, data)) return;
    const found = db.get('restaurants').find({ id: command.data }).value();
    if (found.name) {
      db.get('restaurants').remove({ id: command.data }).write();
      rtm.sendMessage(`${found.name} deleted.`, data.channel);
    } else {
      rtm.sendMessage('Restaurant not found.', data.channel);
    }
  }
};

// listRestaurants lists all restaurant from the database
exports.listRestaurants = (rtm, data, db) => {
  const command = u.checkCommands(data, [
    'food list',
    'makan list ',
    'restaurant list',
    'list food',
    'list makan ',
    'list restaurant',
  ], false);
  if (command) {
    if (!u.isMessageDM(db, data)) return;
    const pageData = parseInt(command.data, 16);
    const limit = 10;
    let page = 1;
    let offsetStart = 0;
    if (pageData && pageData > 1) {
      page = pageData;
      offsetStart = limit*(page-1);
    }
    let offsetEnd = offsetStart + limit + 1;
    let hasNextPage = false;
    const restaurants = db.get('restaurants').slice(offsetStart, offsetEnd).value();
    const rTotal = db.get('restaurants').value().length;
    if (restaurants.length > limit) hasNextPage = true;
    if (restaurants.length > 0) {
      let message = '>>> ';
      restaurants.forEach((r, idx) => {
        message = message + '\n' + u.formatRestaurantStringSimple(r, idx);
      });
      message = message + `\n>Page ${page} of ${Math.ceil(rTotal/limit)}`;
      if (hasNextPage) message = message + `\n>For next page, type \`${command.command} ${page+1}\``;
      rtm.sendMessage(message, data.channel);
    }
  }
};

// randomRestaurant gets a random restaurant
exports.randomRestaurant = (rtm, data, db) => {
  if (u.checkCommands(data, ['eat', 'makan'], true)) {
    const restaurants = db.get('restaurants').value();
    rtm.sendMessage(u.formatRestaurantFindString(u.randomArrayItem(restaurants)), data.channel);
  }
};

exports.test = (rtm, web, data, db) => {
  if (u.textIncludes(data, ['test'])) {
    web.chat.postMessage({
      blocks: [{
        type: 'section',
        text: {
          type: 'mrkdwn',
          text: `Welcome to the channel, <@${data.user}>. We're here to help. Let us know if you have an issue.`,
        },
        accessory: {
          type: 'button',
          text: {
            type: 'plain_text',
            text: 'Get Help',
          },
          value: 'get_help',
        },
      }],
      channel: data.channel,
    });
  }
};
