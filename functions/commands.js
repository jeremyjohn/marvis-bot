const fs = require('fs');
const download = require('image-downloader');
// const moment = require('moment-timezone');
// const { config } = require('../configs/config');
const c = require('../constants');
const u = require('./utils');

// randomAdvice replies with a random advice
exports.randomAdvice = (rtm, web, data, db) => {
  if (u.textIncludes(data, ['advice'])) {
    u.getRequest('https://api.adviceslip.com/advice', (code, res, body) => {
      if (res && res.slip && res.slip.advice) {
        if (u.textIncludes(data, ['all', 'public', 'share', 'every'])) {
          rtm.sendMessage(`>>> <@${data.user}>, ${res.slip.advice}`, data.channel);
        } else if (u.isMessageDM(db, data)) {
          rtm.sendMessage(`>>> <@${data.user}>, ${res.slip.advice}`, data.channel);
        } else {
          web.chat.postEphemeral({
            channel: data.channel,
            text: '>>> ' + res.slip.advice,
            user: data.user,
          });
        }
      }
    });
  }
};

// randomFoxPicture uploads a random fox picture
exports.randomFoxPicture = async (rtm, web, data, db) => {
  if (u.checkCommands(data, ['fox'], false)) {
    const processingTS = await u.pMessage(web, data, ':fox_face: Finding a fox...');
    u.getRequest('https://randomfox.ca/floof/', (code, res, body) => {
      if (code === 200 && res && res.image) {
        const timestamp = +new Date();
        const filename = `fox_${timestamp}.jpg`;
        const title = 'What does the fox say?';
        const initialComment = `>>> <@${data.user}>, Fox found.`;
        const dest = 'tmp/'+filename;
        download.image({
          url: res.image,
          dest,
        }).then(() => {
          web.files.upload({
            title,
            filename,
            channels: data.channel,
            initial_comment: initialComment,
            file: fs.createReadStream(dest),
          }).then(() => {
            fs.unlinkSync(dest);
            u.deleteMessage(web, data.channel, processingTS);
          }).catch(err => {
            console.log(err);
            fs.unlinkSync(dest);
            u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
          });
        }).catch(err => {
          console.log(err);
          u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
        });
      } else {
        u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
      }
    });
  }
};

// randomDogePicture uploads a random doge/shibe picture
exports.randomDogePicture = async (rtm, web, data, db) => {
  if (u.checkCommands(data, ['doge', 'shibe'], false)) {
    const processingTS = await u.pMessage(web, data, ':dog2: Finding a doge...');
    u.getRequest('http://shibe.online/api/shibes', (code, res, body) => {
      if (code === 200 && res && res.length > 0) {
        const timestamp = +new Date();
        const filename = `doge_${timestamp}.jpg`;
        const title = 'Much Such very wow';
        const initialComment = `>>> <@${data.user}>, Doge found.`;
        const dest = 'tmp/'+filename;
        download.image({
          url: res[0],
          dest,
        }).then(() => {
          web.files.upload({
            title,
            filename,
            channels: data.channel,
            initial_comment: initialComment,
            file: fs.createReadStream(dest),
          }).then(() => {
            fs.unlinkSync(dest);
            u.deleteMessage(web, data.channel, processingTS);
          }).catch(err => {
            console.log(err);
            fs.unlinkSync(dest);
            u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
          });
        }).catch(err => {
          console.log(err);
          u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
        });
      } else {
        u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
      }
    });
  }
};

// randomCatPicture uploads a random cat/kitty picture
exports.randomCatPicture = async (rtm, web, data, db) => {
  if (u.checkCommands(data, ['cat', 'kitty', 'kitten'], false)) {
    const processingTS = await u.pMessage(web, data, ':cat2: Finding a kitty...');
    u.getRequest('https://aws.random.cat/meow', (code, res, body) => {
      if (code === 200 && res && res.file) {
        const timestamp = +new Date();
        const filename = `cat_${timestamp}.jpg`;
        const title = 'Meow meow';
        const initialComment = `>>> <@${data.user}>, Kitty found.`;
        const dest = 'tmp/'+filename;
        download.image({
          url: res.file,
          dest,
        }).then(() => {
          web.files.upload({
            title,
            filename,
            channels: data.channel,
            initial_comment: initialComment,
            file: fs.createReadStream(dest),
          }).then(() => {
            fs.unlinkSync(dest);
            u.deleteMessage(web, data.channel, processingTS);
          }).catch(err => {
            console.log(err);
            fs.unlinkSync(dest);
            u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
          });
        }).catch(err => {
          console.log(err);
          u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
        });
      } else {
        u.updateMessage(web, data.channel, processingTS, c.messages.ERROR_OCCURRED);
      }
    });
  }
};
