const request = require('request');
const moment = require('moment-timezone');
const Hashids = require('hashids');
const { config } = require('../configs/config');

// cb is a callBack helper
exports.cb = (callBack, ...values) => {
  if (callBack) callBack(...values);
};

// getRequest create a GET request to the api
exports.getRequest = (url, callback) => {
  const t0 = +new Date();
  // Run GET request
  let headers = {
    'Content-Type': 'application/json',
  };
  let options = {
    url,
    method: 'GET',
    headers: headers,
  };
  request(options, function (error, response, body) {
    const t1 = +new Date();
    console.log(moment().tz('Asia/Singapore').format('YYYY/MM/DD h:mm:ss'), 'GET ->', url, ' '+(t1 - t0)+'ms');
    let responseData;
    let responseBody = body;
    if (error) {
      console.log(error);
    } else {
      if (response.statusCode !== 200) {
        console.log(response.statusCode, body);
      } else {
        try {
          responseData = JSON.parse(body);
        } catch (e) {
          console.log(e);
        }
      }
      if (callback) callback(response.statusCode, responseData, responseBody);
    }
  });
};

// millisToMinutesAndSeconds convertes milliseconds to minutes and seconds
exports.millisToMinutesAndSeconds = (millis) => {
  var minutes = Math.floor(millis / 60000);
  var seconds = ((millis % 60000) / 1000).toFixed(1);
  if (minutes > 0) {
    seconds = ((millis % 60000) / 1000).toFixed(0);
    return minutes + '.' + (seconds < 10 ? '0' : '') + seconds + ' minutes(s)';
  }
  return seconds + ' seconds(s)';
};

// toTitleCase string to title case
exports.toTitleCase = (str) => {
  return str.replace(
    /\w\S*/g,
    function (txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    }
  );
};

// asyncForEach helps to await for array
exports.asyncForEach = async (array, callback) => {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
};

// isMessageDM check if message is sent from DM or channel
exports.isMessageDM = (db, data) => {
  let isDM = false;
  const imList = db.get('imList').value();
  imList.forEach(im => {
    if (im.id === data.channel) isDM = true;
  });
  return isDM;
};

// checkCommands check if message content has these commands and return the extra data;
exports.checkCommands = (data, commands = [], includePrefix = true) => {
  let hasCommand = false;
  let content = '';
  if (data.text) content = data.text.toLowerCase();
  let prefix = '';
  if (includePrefix) prefix = config.prefix;
  commands.forEach(c => {
    if (content.startsWith(prefix+c.toLowerCase())) {
      hasCommand = {
        match: true,
        data: '',
        command: c.toLowerCase(),
      };
      if (content.startsWith(prefix+c.toLowerCase()+' ', '')) {
        hasCommand.data = data.text.substr((prefix+c).length).trim();
      }
    }
  });
  return hasCommand;
};

// textIncludes check if message content has these text and return bool;
exports.textIncludes = (data, commands = []) => {
  let hasCommand = false;
  let content = '';
  if (data.text) content = data.text.toLowerCase();
  commands.forEach(c => {
    if (content.includes(c.toLowerCase())) {
      hasCommand = true;
    }
  });
  return hasCommand;
};

// generateShortID generates a unique short id
exports.generateShortID = () => {
  const hashids = new Hashids();
  return hashids.encode(new Date().getTime(), Math.floor(Math.random() * 10));
};

// randomArrayItem returns random item from array
exports.randomArrayItem = (array = []) => {
  return array[Math.floor(Math.random() * array.length)];
};

// restaurantStruct creates the restaurant struct
exports.restaurantStruct = (data = {}) => {
  return Object.assign({
    id: exports.generateShortID(),
    name: '',
    location: '',
    locationType: '',
    parking: '',
    halal: '',
    cheap: false,
    aircond: false,
  }, data);
};

// formatRestaurantString creates the restaurant in string format
exports.formatRestaurantString = (restaurant) => {
  let message = `>>> *ID:* ${restaurant.id}`;
  message = message + `\n*Name:* ${restaurant.name}`;
  message = message + `\n*Location:* ${restaurant.location}`;
  message = message + `\n*Location Type:* ${restaurant.locationType}`;
  message = message + `\n*Halal:* ${restaurant.halal}`;
  message = message + `\n*Cheap:* ${restaurant.cheap ? 'Yes' : 'No'}`;
  message = message + `\n*Aircond:* ${restaurant.aircond ? 'Yes' : 'No'}`;
  message = message + `\n*Parking Difficulty:* ${restaurant.parking}`;
  return message;
};

// formatRestaurantString creates the restaurant in simple string format
exports.formatRestaurantStringSimple = (restaurant, idx) => {
  return `*${restaurant.name}* \`${restaurant.id}\``;
};

// formatRestaurantFindString formats the restaurant data
exports.formatRestaurantFindString = (restaurant) => {
  let message = `*${restaurant.name}*`;
  message = message + `\n>>> *Location:* ${restaurant.location}`;
  message = message + `\n*Location Type:* ${restaurant.locationType}`;
  message = message + `\n*Halal:* ${restaurant.halal}`;
  message = message + `\n*Cheap:* ${restaurant.cheap ? 'Yes' : 'No'}`;
  message = message + `\n*Aircond:* ${restaurant.aircond ? 'Yes' : 'No'}`;
  message = message + `\n*Parking Difficulty:* ${restaurant.parking}`;
  return message;
};

// pMessage sends the processing message
exports.pMessage = async (web, data, customMessage) => {
  const msg = await web.chat.postMessage({
    channel: data.channel,
    text: `:hourglass_flowing_sand: ${customMessage || 'Processing...'}`,
  });
  return msg.ts;
};

// deleteMessage deletes message by channel & ts
exports.deleteMessage = async (web, channel, ts) => {
  await web.chat.delete({
    channel,
    ts,
  });
};

// updateMessage updates message by channel & ts
exports.updateMessage = async (web, channel, ts, text) => {
  await web.chat.update({
    channel,
    text,
    ts,
  });
};
