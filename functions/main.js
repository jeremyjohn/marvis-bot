// imports
const c = require('./commands');
const r = require('./restaurant');

exports.runMain = (rtm, web, data, db) => {
  // run functions
  // if (!blockedChannelsIDs.find(bc => bc === rtm.channel)) {
  // }
  // miscellaneous commands
  c.randomAdvice(rtm, web, data, db);
  c.randomFoxPicture(rtm, web, data, db);
  c.randomDogePicture(rtm, web, data, db);
  c.randomCatPicture(rtm, web, data, db);
  // restaurant commands
  r.addRestaurant(rtm, data, db);
  r.updateRestaurant(rtm, data, db);
  r.deleteRestaurant(rtm, data, db);
  r.listRestaurants(rtm, data, db);
  r.randomRestaurant(rtm, data, db);
  r.test(rtm, web, data, db);
};
