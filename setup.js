const fs = require('fs');
// config
fs.open('./configs/config.js', 'r', function (err, file) {
  if (err) {
    // read the example file
    fs.readFile('./configs/config.example.js', function (err, exampleFile) {
      // console.log(exampleFile.toString('utf8'));
      if (err) throw err;
      // create the new role file
      fs.writeFile('./configs/config.js', exampleFile.toString('utf8'), function (err) {
        if (err) throw err;
        console.log('Config file create, please edit it.');
      });
    });
  } else {
    console.log('Config file already exists.');
  }
});
